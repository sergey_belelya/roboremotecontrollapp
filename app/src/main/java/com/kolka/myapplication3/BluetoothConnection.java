package com.kolka.myapplication3;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.util.Log;

import java.util.Set;

class BluetoothConnection {

    private BluetoothAdapter bluetoothAdapter;
    private Set<BluetoothDevice> devices;

    BluetoothConnection() {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    boolean isSupported() {
        return bluetoothAdapter != null;
    }

    public void disableBluetooth() {
        bluetoothAdapter.disable();
    }

    boolean searchDevices() {
        if (bluetoothAdapter.isEnabled()) {
            devices = bluetoothAdapter.getBondedDevices();

            if (bluetoothAdapter.isDiscovering()) {
                bluetoothAdapter.cancelDiscovery();
                bluetoothAdapter.startDiscovery();
            } else {
                bluetoothAdapter.startDiscovery();
            }


            return true;
        }

        return false;
    }

    boolean isDiscovering() {
        return bluetoothAdapter.isDiscovering();
    }

    boolean isEnable() {
        return bluetoothAdapter.isEnabled();
    }

    boolean stopScanning() {
        return bluetoothAdapter.cancelDiscovery();
    }

    public Set<BluetoothDevice> getDevices() {
        return devices;
    }
}
