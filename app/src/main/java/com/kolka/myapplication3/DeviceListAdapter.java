package com.kolka.myapplication3;

import java.util.ArrayList;

import android.content.Context;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.TextView;


class DeviceListAdapter extends ArrayAdapter<Device> {
    private LayoutInflater inflater;

    DeviceListAdapter(Context context, ArrayList<Device> devices) {
        super(context, 0, devices);
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        Device device = getItem(position);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.device_list_item, parent, false);

            holder = new ViewHolder();

            holder.nameTv = (TextView) convertView.findViewById(R.id.device_list_item_deviceName);
            holder.addressTv = (TextView) convertView.findViewById(R.id.device_list_item_macAddress);
            holder.pair = (TextView) convertView.findViewById(R.id.device_list_item_pairedDevice);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.nameTv.setText(device.getNameDevice());
        holder.addressTv.setText(device.getMacAddressDevice());

        if (device.isPaired())
            holder.pair.setText("Сопряжен");
        else
            holder.pair.setText("Не сопряжен");

        return convertView;
    }

    private class ViewHolder {
        TextView nameTv;
        TextView addressTv;
        TextView pair;
    }
}
