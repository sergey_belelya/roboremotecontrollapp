package com.kolka.myapplication3;

import android.bluetooth.BluetoothDevice;


import java.util.ArrayList;

/*
* Класс описывает статичный массив устройств
*/
class ArrayDevice  {

    private static ArrayDevice arrayDevice;
    private static ArrayList<Device> devices;

    private ArrayDevice() {
        devices = new ArrayList<>();
    }

    static void init() {
        if (arrayDevice == null)
            arrayDevice = new ArrayDevice();
    }

    static ArrayList<Device> getDevices() {
        return devices;
    }

    /*Метод ищет устройство по адресу*/
    static Device getDevice(String mac) {
        if (arrayDevice != null) {
            for (Device device : devices) {
                if (device.getMacAddressDevice().equals(mac))
                    return device;
            }
        }
        return null;
    }

    static void clear(){
        devices.clear();
    }

    static int size(){
        return devices.size();
    }

    static void addDevice(BluetoothDevice device){
        if (arrayDevice != null)
            devices.add(new Device(device));
    }
}
