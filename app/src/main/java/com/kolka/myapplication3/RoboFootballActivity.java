package com.kolka.myapplication3;


import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.jmedeisis.bugstick.Joystick;
import com.jmedeisis.bugstick.JoystickListener;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

/**
 * Класс описывает активити с джойстиком для управления роботом
 */
public class RoboFootballActivity extends AppCompatActivity {

    /*Идентификатор андроид-устройства*/
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    /**
     * 4х-байтовое сообщения для отправки устройству
     * 0 - инициализирущий байт
     * 1 - кнопки
     * 2 - вперед/назад
     * 3 - влево/вправо
     */
    private byte[] message = new byte[4];

    private BluetoothSocket btSocket;
    private OutputStream outStream;

    private Device device;

    private boolean btn1Pressed = false;
    private boolean btn2Pressed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Начальная инициализация сообщения
         */
        message = new byte[]{(byte) 0xFF, (byte) 0x00, (byte) 0x7F, (byte) 0x7F};

        /**
         * Получаем выбранное в списке устройство
         */
        device = ArrayDevice.getDevice(getIntent().getStringExtra("device"));

        setContentView(R.layout.activity_robofootball);

        Button btn1 = (Button) findViewById(R.id.btn1);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btn1Pressed) {
                    message[1] &= 2;
                    btn1Pressed = false;
                } else {
                    message[1] |= 1;
                    btn1Pressed = true;
                }

                //Log.d("TAG", "Second: " + message[1]);
            }
        });

        Button btn2 = (Button) findViewById(R.id.btn2);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btn2Pressed) {
                    message[1] &= 1;
                    btn2Pressed = false;
                } else {
                    message[1] |= 2;
                    btn2Pressed = true;
                }

                //Log.d("TAG", "Second: " + message[1]);
            }
        });

        /**
         * Инициализируем джойстик
         */
        Joystick joystick = (Joystick) findViewById(R.id.joystick);
        joystick.setJoystickListener(new JoystickListener() {

            /**
             * Нажатие на джойстик
             */
            @Override
            public void onDown() {

            }

            /**
             * Движение при нажатии
             * @param degrees - угол в градусах.
             *                - От 0 до 180* в 1 и 2 четверти
             *                - От -180 до 0 в 3 и 4 четверти
             * @param offset - сила сдвига джойстика. От 0 до 1.
             */
            @Override
            public void onDrag(float degrees, float offset) {
                calculate(degrees, offset);
            }

            /**
             * Отпускание пальца с джойстика
             * Здесь сбрасываем значения
             */
            @Override
            public void onUp() {
                message[2] = (byte) 0x7F;
                message[3] = (byte) 0x7F;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stop();
    }

    /**
     * Закрываем поток и сокет
     */
    private void stop() {
        if (outStream != null) {
            try {
                outStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
                killActivity();
            }
        }

        if (btSocket != null) {
            try {
                btSocket.close();
            } catch (IOException e2) {
                e2.printStackTrace();
                killActivity();
            }
        }
    }

    /**
     * Создаем сокеты и поток.
     * Каждые 100 мс передаем сообщение устройству
     */
    private void start() {
        BluetoothDevice bluetoothDevice = device.getBluetoothDevice();

        try {
            btSocket = bluetoothDevice.createRfcommSocketToServiceRecord(MY_UUID);
        } catch (IOException ex) {
            ex.printStackTrace();
            killActivity();
        }

        try {
            btSocket.connect();
        } catch (IOException e) {
            try {
                btSocket.close();
            } catch (IOException e2) {
                e2.printStackTrace();
                killActivity();
            }

            killActivity();
        }

        try {
            outStream = btSocket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
            killActivity();
        }

        Timer myTimer = new Timer();
        myTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (btSocket.isConnected()) {
                    byte[] msgBuffer = message;

                    try {
                        outStream.write(msgBuffer);
                    } catch (IOException ex) {
                        killActivity();
                    }
                }
            }
        }, 1000, 100);
    }

    /**
     * Метод вычисляет скорость и направление перемещения.
     * Составляется пропорция для скорости в двух осях
     *
     * @param degrees - угол в градусах
     * @param offset  - сила сдвига стика
     */
    private void calculate(float degrees, float offset) {

        /**
         * Вычисляем пропорции от угла направления по оси Y: sin(a)^2
         */
        double y = Math.pow(Math.sin(Math.toRadians(Math.abs(degrees))), 2);
        double x = 1 - y;

        /**
         * Вычисляем скорость
         */
        byte mess_x = power(x * 255 * offset);
        byte mess_y = power(y * 255 * offset);

        if (degrees > 0 && degrees < 90) {
            mess_x += (byte) 127;
            mess_y += (byte) 127;
        } else if (degrees > 90 && degrees < 180) {
            mess_x = (byte) (127 - mess_x);
            mess_y += (byte) 127;
        } else if (degrees < 0 && degrees > -90) {
            mess_x += (byte) 127;
            mess_y = (byte) (127 - mess_y);
        } else if (degrees < -90 && degrees > -180) {
            mess_x = (byte) (127 - mess_x);
            mess_y = (byte) (127 - mess_y);
        }

        message[2] = mess_y;
        message[3] = mess_x;
    }

    /**
     * Метод возвращает байтовое представление числа
     * в ограниченном диапазоне
     *
     * @param value - число
     * @return - байт
     */
    private byte power(double value) {
        int buff = (int) value;
        if (buff < 127)
            return (byte) buff;
        else
            return (byte) 127;
    }

    /*Метод завершает активити*/
    private void killActivity() {
        Toast.makeText(getBaseContext(), "Ошибка. Выбранное устройство не поддерживается", Toast.LENGTH_LONG).show();
        finish();
    }
}
