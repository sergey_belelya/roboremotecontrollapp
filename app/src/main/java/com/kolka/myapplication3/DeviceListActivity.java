package com.kolka.myapplication3;

import java.lang.reflect.Method;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.os.Bundle;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Класс описывает активити со списком устройств
 */
public class DeviceListActivity extends AppCompatActivity {

    /**
     * Адаптер списка
     */
    private DeviceListAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);

        /**
         * Получаем список устройств
         */
        ArrayList<Device> deviceList = ArrayDevice.getDevices();

        ListView listView = (ListView) findViewById(R.id.lv_paired);

        /**
         * Инициализируем адаптер и передаём ему список устройств
         */
        adapter = new DeviceListAdapter(this, deviceList);

        /**
         * Инициализируем список устройств.
         */
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Device device = (Device) parent.getItemAtPosition(position);
                connectDevice(device);
            }
        });

        /*Инициализируем интент-фильтр*/
        /*
      Фильтр интента
     */
        IntentFilter filter = new IntentFilter();
        /*Фильтр изменения состояния bluetooth*/
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        /*Фильтр изменения статуса сопряжения*/
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
    }

    /**
     * Метод для соединения с устройством
     * При необходимости - происходит сопряжение
     * @param device - устройство, с которым устанавливается соединение
     */
    private void connectDevice(final Device device) {
        final boolean paired = device.isPaired();

        String message;
        if (paired) {
            message = "Выбрать " + device.getNameDevice() + "?";
        } else {
            message = "Соединится с " + device.getNameDevice() + "?";
        }

        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Подключение");
        adb.setMessage(message);
        adb.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (paired) {
                    goToAction(device);
                } else {
                    pairDevice(device.getBluetoothDevice());
                }
            }
        });

        adb.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        adb.show();

        //registerReceiver(pairReceiver, filter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        /**
         * Отменяем регистрацию приемника интентов
         */
        //unregisterReceiver(pairReceiver);
    }

    /**
     * Переход к активити с выбором действия с устройством
     * @param device - устройство, с которым устанавливается соединение
     */
    private void goToAction(Device device) {
        Intent intent = new Intent(getApplicationContext(), RoboFootballActivity.class);
        intent.putExtra("device", device.getMacAddressDevice());
        startActivity(intent);
    }

    /**
     * Метод выводит сообщение внизу экрана
     * @param message - текст сообщения
     */
    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Метод вызывается для сопряжения с устройством
     */
    private void pairDevice(BluetoothDevice device) {
        try {
            Method method = device.getClass().getMethod("createBond", (Class[]) null);
            method.invoke(device, (Object[]) null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод вызывается для отмены сопряжения с устройством
     */
    private void unpairDevice(BluetoothDevice device) {
        try {
            Method method = device.getClass().getMethod("removeBond", (Class[]) null);
            method.invoke(device, (Object[]) null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Receiver - приемник интентов от системы.
     * В данном приемнике отлавливаются результаты работы bluetooth
     */
    private final BroadcastReceiver pairReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
                final int prevState = intent.getIntExtra(BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE, BluetoothDevice.ERROR);

                BluetoothDevice bluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                Device device = ArrayDevice.getDevice(bluetoothDevice.getAddress());

                if (state == BluetoothDevice.BOND_BONDED && prevState == BluetoothDevice.BOND_BONDING) {
                    if (device != null) {
                        int index = ArrayDevice.getDevices().indexOf(device);
                        ArrayDevice.getDevices().get(index).setPaired(true);
                        goToAction(device);
                    }

                } else if (state == BluetoothDevice.BOND_NONE && prevState == BluetoothDevice.BOND_BONDED) {
                    if (device != null) {
                        int index = ArrayDevice.getDevices().indexOf(device);
                        ArrayDevice.getDevices().get(index).setPaired(false);
                    }
                }

                /**
                 * Обновляем адаптер списка (т.е. обновляем содержимое списка)
                 */
                adapter.notifyDataSetChanged();
            }

            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                if (state == BluetoothAdapter.STATE_OFF){
                    showToast("Bluetooth выключен");
                    killActivity();
                }
            }
        }
    };

    /**
     * Метод завершает активити
     */
    private void killActivity(){
        finish();
    }
}
