package com.kolka.myapplication3;

import android.bluetooth.BluetoothDevice;

/**
 * Класс описывает одно устройство
 */
class Device {

    private BluetoothDevice bluetoothDevice;
    private String nameDevice;
    private String macAddressDevice;

    /**
     * Статус сопряжения устройства
     */
    private boolean paired;

    Device(BluetoothDevice device){
        setBluetoothDevice(device);
        setNameDevice(device.getName());
        setMacAddressDevice(device.getAddress());

        if (device.getBondState() == BluetoothDevice.BOND_BONDED)
            setPaired(true);
        else
            setPaired(false);
    }

    String getNameDevice() {
        return nameDevice;
    }

    private void setNameDevice(String nameDevice) {
        this.nameDevice = nameDevice;
    }

    String getMacAddressDevice() {
        return macAddressDevice;
    }

    private void setMacAddressDevice(String macAddressDevice) {
        this.macAddressDevice = macAddressDevice;
    }

    boolean isPaired() {
        return paired;
    }

    void setPaired(boolean paired) {
        this.paired = paired;
    }

    BluetoothDevice getBluetoothDevice() {
        return bluetoothDevice;
    }

    private void setBluetoothDevice(BluetoothDevice bluetoothDevice) {
        this.bluetoothDevice = bluetoothDevice;
    }
}
