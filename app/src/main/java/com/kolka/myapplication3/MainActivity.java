package com.kolka.myapplication3;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    /**
     * Константа используется в получении разрешении
     * на включение bluetooth в Android >= 6.0
     */
    private int REQUEST_COARSE_LOCATION_PERMISSIONS;

    private ProgressDialog mProgressDlg;
    private BluetoothConnection bluetoothConnection;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**
         * Инициализируем вспомогательные классы
         * ArrayDevice - статичный массив устройств
         * BluetoothConnection - класс-контроллер, для управления bluetooth'ом
         * */
        ArrayDevice.init();
        bluetoothConnection = new BluetoothConnection();

        Button btnSearch = (Button) findViewById(R.id.fragment_start_btn_search);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * Запускаем поиск устройств
                 */
                start();
            }
        });

        /**
         * Настройка диалога поиска
         */
        mProgressDlg = new ProgressDialog(this);
        mProgressDlg.setMessage("Сканирование");
        mProgressDlg.setCancelable(false);
        mProgressDlg.setButton(DialogInterface.BUTTON_NEGATIVE, "Остановить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bluetoothConnection.stopScanning();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        /**
         * Инициализируем интент-фильтр
         * ACTION_STATE_CHANGED - фильтр изменения состояния bluetooth
         * ACTION_FOUND - фильтр обнаружения нового устройства
         * ACTION_DISCOVERY_STARTED - фильтр запуска поиска устройств
         * ACTION_DISCOVERY_FINISHED - фильтр остановки поиска устройств
         */
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);

        registerReceiver(receiver, filter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        /**
         * Выключаем поиск, если включен
         */
        if (bluetoothConnection.isSupported()) {
            if (bluetoothConnection.isDiscovering()) {
                bluetoothConnection.stopScanning();
            }
        }

        unregisterReceiver(receiver);
    }

    /**
     * Метод сначала запрашивает разрешение на использование bluetooth
     * а затем начинает поиск устройств. Вторая часть метода необходима для
     * запроса разрешения в версии андроида > 6.0
     */
    private void start() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            startSearch();
        } else {
            ActivityCompat.requestPermissions(
                    MainActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_COARSE_LOCATION_PERMISSIONS
            );
        }
    }

    /**
     * Получаем результат разрешение на включение bluetooth
     * Для Android > 6.0
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == REQUEST_COARSE_LOCATION_PERMISSIONS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startSearch();
            } else {
                showToast("Доступ запрещен");
            }
        }
    }

    /**
     * Метод запускает поиск устройств.
     * Если устройство не поддерживает bluetooth - выводим сообщение. Поиск не производится.
     * Если bluetooth включен, то стартует поиск, иначе - предложение включить bluetooth
     * с последующим стартом поиска.
     */
    private void startSearch() {
        if (!bluetoothConnection.isSupported()) {
            showUnsupported();
        } else {
            if (bluetoothConnection.isEnable()) {
                bluetoothConnection.searchDevices();
            } else {
                enableBluetooth();
            }
        }
    }

    private void showUnsupported() {
        showToast("Bluetooth не поддерживается этим устройством");
    }

    /**
     * Диалог на запуск bluetooth
     */
    private void enableBluetooth() {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Предупреждение");
        adb.setMessage("Bluetooth не задействован. Включить?");
        adb.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                /*Запускаем bluetooth*/
                Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(intent, 1000);
            }
        });

        adb.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                showToast("Bluetooth не включен");
            }
        });

        adb.show();
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();

                //Log.d("TAG", "Action: " + action);

                /**
                 * Условие истинно, если поменялся статус работы bluetooth
                 */
                if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                    final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                /*Запускаем поиск при включении bluetooth*/
                    if (state == BluetoothAdapter.STATE_ON) {
                        bluetoothConnection.searchDevices();
                    }

                /*Прекращаем поиск по выключении bluetooth*/
                    if (state == BluetoothAdapter.STATE_OFF){
                        mProgressDlg.dismiss();
                        bluetoothConnection.stopScanning();
                        showToast("Bluetooth выключен");
                    }
                }

                /**
                 * Условие истинно, если запустился поиск устройств
                 * При старте поиска очищаем массив утройств и показываем диалог
                 */
                if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                        showToast("Запуск сканирования...");
                        ArrayDevice.clear();
                        mProgressDlg.show();
                }

                /**
                 * Условие истинно, если остановился поиск устройств
                 * При прекращении поиска запускаем активити со списком устройтв.
                 * Если устройства не обнаружены, то выводим сообщение
                 */
                if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                    mProgressDlg.dismiss();

                    if (ArrayDevice.size() == 0)
                        showToast("Устройства не найдены.");
                    else {
                        showToast("Сканирование завершено");
                        Intent newIntent = new Intent(MainActivity.this, DeviceListActivity.class);
                        startActivity(newIntent);
                    }
                }

                /**
                 * Условие истинно, если найдено новое устройство
                 * Каждое найденное устройство добавляем в массив устройст
                 * И выводим сообщение с именем найденым устройства
                 */
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    if (device.getName() != null && device.getAddress() != null) {
                        showToast("Найдено устройство " + device.getName());
                        ArrayDevice.addDevice(device);
                    }
                }
            }
        };

    /**
     * Метод выводит сообщение внизу экрана
     * @param message - текст сообщения
     */
    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
